export function getComponentTemplates(
    componentName:string, filenName:string, componentType:string, moduleName:string
) {
    switch (componentType) {
        case 'field':
            return fieldComponentTemplate(componentName, filenName, moduleName)
        case 'action':
            return actionComponentTemplate(componentName, filenName, moduleName)
            break;
        case 'common':
            return commonComponentTemplate(componentName, moduleName)
        default:
            return {jsTemplate: '', xmlTemplate: ''}
    }
}

function commonComponentTemplate(componentName:string, moduleName:string) {
    const jsTemplate = `/** @odoo-module **/

import { Component } from "@odoo/owl";

export class ${componentName} extends Component {
    setup() {

    }
}

${componentName}.template = "${moduleName}.${componentName}";
${componentName}.components = {};

${componentName}.defaultProps = {};
${componentName}.props = {};`

    const xmlTemplate = `<?xml version="1.0" encoding="utf-8"?>
<templates xml:space="preserve">
    <t t-name="${moduleName}.${componentName}" owl="1">
    </t>
</templates>`
    return { jsTemplate, xmlTemplate }
}

function fieldComponentTemplate(componentName:string, widgetName:string, moduleName:string) {
    if(widgetName.endsWith('_field')) {
        widgetName = widgetName.slice(0, -6);
    }
    const jsTemplate = `/** @odoo-module **/

import { registry } from "@web/core/registry";
import { standardFieldProps } from "@web/views/fields/standard_field_props";

export class ${componentName} extends Component {
    setup() {

    }
}

${componentName}.template = "${moduleName}.${componentName}";
${componentName}.components = {};

${componentName}.props = {
    ...standardFieldProps,
};
${componentName}.defaultProps = {};
${componentName}.extractProps = ({ attrs }) => {
    return {

    };
};

${componentName}.displayName = "";
${componentName}.supportedTypes = [""];

registry.category("fields").add("${widgetName}", ${componentName});
`

    const xmlTemplate = `<?xml version="1.0" encoding="utf-8"?>
<templates xml:space="preserve">
    <t t-name="${moduleName}.${componentName}" owl="1">
    </t>
</templates>`
    return { jsTemplate, xmlTemplate }
}

function actionComponentTemplate(componentName:string, actionName:string, moduleName:string) {
    const jsTemplate = `/** @odoo-module **/

import { registry } from "@web/core/registry";
import { Layout } from "@web/search/layout";
import { getDefaultConfig } from "@web/views/view";

import { Component, useSubEnv } from "@odoo/owl";

export class ${componentName} extends Component {
    setup() {
        useSubEnv({
            config: {
                ...getDefaultConfig(),
                ...this.env.config,
            },
        });
    }
}

${componentName}.template = "${moduleName}.${componentName}";
${componentName}.components = { Layout };

registry.category("actions").add("${actionName}", ${componentName});`

    const xmlTemplate = `<?xml version="1.0" encoding="utf-8"?>
<templates xml:space="preserve">
    <t t-name="${moduleName}.${componentName}" owl="1">
        <div class="o_action">
            <Layout display="{
                controlPanel: {
                    'top-right' : false,
                    'top-left' : false,
                    'bottom-right': false,
                    'bottom-left': false,
                }
            }">
            </Layout>
        </div>
    </t>
</templates>`
    return { jsTemplate, xmlTemplate }
}