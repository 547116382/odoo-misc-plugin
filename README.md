# 功能简介
插件支持的功能：
- `创建Odoo模块`
- `创建Model`，根据模型名称创建Python代码以及视图代码
- `继承Model`，根据模型名称创建Python代码
- `创建OWL组件`，在目录`static/src/components`下创建组件代码模板，根据选择的类型生成不同的组件模板，支持的类型：action/field/common
- `创建Web服务`，在`static/src/services`目录下生成服务代码模板
- `获取导入路径`，选择Odoo模块`static/src/`目录下的js文件，可以自动生成导入路径
- `打开导入文件`，使用该功能需要先在vscode中配置Odoo模块的路径
- 提供一些代码片段。vscode插件市场中有很多关于odoo代码片段的插件（搜索关键词`odoo snippets`）
  - `owhk`: 导入`OWL`中定义的`Hooks`
  - `owlc`: 导入`OWL`的声明周期函数
  - `ohk`: 导入`Odoo Web`中定义的`Hooks`（不是全部，只导入一些比较常见的）
  - `opatch`: 对组件进行`patch`

# 使用说明
右键单击对应的目录/文件，在弹出菜单的底部选择`Odoo Misc`菜单，然后单击具体的子菜单。
- `创建Odoo模块`：单击模块需要存放的目录
- `创建Model`，单击Odoo模块目录
- `继承Model`，单击Odoo模块目录
- `创建OWL组件`，单击Odoo模块目录
- `创建Web服务`，单击Odoo模块目录
- `获取导入路径`，单击Odoo模块`static/src/`目录下的js文件

## `创建Odoo模块`
点击菜单后，会弹出提示框
1. 根据提示，输入模块名称，模块名称如果包含多个单词，使用`_`进行连接，例如`sale_order`
2. 之后选择模块对应的Odoo版本，不同的版本的`__manifest__.py`文件会有所不同
3. 最后选择是否包含前端内容，如果包含前端内容，会生成`static`目录，以及在`__manifest__.py`文件中导入相关路径

模块的目录结构：
```text
sale_order/
|-- models/
|   |-- __init__.py
|-- security/
|   |-- ir.model.access.csv
|-- static/
|   |-- description/
|   |-- src/
|-- views/
|-- wizard/
|   |-- __init__.py
|-- __init__.py
|-- __manifest__.py
```

## `创建Model`
点击菜单后，在弹出的输入框中输入模型名称，模型名称使用`.`分割，例如`sale.order`。输入完成后点击回车，即可生成模型的python代码、视图代码，还会在`ir.model.access.csv`文件中添加权限，视图文件与Python文件会自动添加到`__manifest__.py`与`models/__init__.py`中。
- 注意：`models/__init__.py`与`ir.model.access.csv`文件末尾需要保留空行

生成的`models/sale_order.py`文件
```py
import logging

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

_logger = logging.getLogger(__name__)


class SaleOrder(models.Model):
    """
    """
    _name = 'sale.order'
    _description = ''
```

生成的`views/sale_order_views.xml`文件
```xml
<?xml version="1.0" encoding="utf-8"?>
<odoo>
    <!-- sale.order 列表视图 -->
    <record id="sale_order_view_tree" model="ir.ui.view">
        <field name="name">sale.order.view.tree</field>
        <field name="model">sale.order</field>
        <field name="arch" type="xml">
            <tree>

            </tree>
        </field>
    </record>
    <!-- sale.order 表单视图 -->
    <record id="sale_order_view_form" model="ir.ui.view">
        <field name="name">sale.order.view.form</field>
        <field name="model">sale.order</field>
        <field name="arch" type="xml">
            <form string="">
                <header></header>
                <sheet>
                    <group string="隐藏字段" invisible="1">
                        <field name="id"/>
                    </group>
                    <group>
                        <group>

                        </group>
                        <group>
                        </group>
                    </group>
                </sheet>
            </form>
        </field>
    </record>
    <!-- sale.order 搜索视图 -->
    <record id="sale_order_view_search" model="ir.ui.view">
        <field name="name">sale.order.view.search</field>
        <field name="model">sale.order</field>
        <field name="arch" type="xml">
            <search string="">
                <field name=""/>
                <filter string="当前月份" name="this_month" domain="[('时间字段', '&gt;=', context_today().strftime('%Y-%m-01'))]"/>
            </search>
        </field>
    </record>
    <!-- sale.order.act_window -->
    <record id="sale_order_action" model="ir.actions.act_window">
        <field name="name"></field>
        <field name="type">ir.actions.act_window</field>
        <field name="res_model">sale.order</field>
        <field name="view_mode">tree,form</field>
        <field name="target">current</field>
    </record>
</odoo>
```

`ir.model.access.csv`文件自动写入内容（最后的空行不要删除）
```csv
id,name,model_id/id,group_id/id,perm_read,perm_write,perm_create,perm_unlink
access_sale_order_user,sale_order_user,model_sale_order,,1,1,1,0
access_sale_order_manager,sale_order_manager,model_sale_order,,1,1,1,1
,,,,,,,

```

## `继承Model`
点击菜单后，在弹出的输入框中输入要继承的模型名称，以`.`分割，例如`product.template`，输入完成后，点击回车，即可生成模型的Python代码，以及自动写入到`models/__init__.py`文件中

生成的代码：
```py
import logging

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

_logger = logging.getLogger(__name__)


class ProductTemplate(models.Model):
    """
    """
    _inherit = 'product.template'

```

## `创建OWL组件`
点击菜单后，在弹出的输入框中输入组件名称，名称以空格进行分割，例如`image field`。之后选择组件模板类型，比如选择`field`类型，最终生成的代码：
* 如果模块下没有`static`路径，无需手动创建，插件会自动创建
```js
/** @odoo-module **/

import { registry } from "@web/core/registry";
import { standardFieldProps } from "@web/views/fields/standard_field_props";

export class ImageField extends Component {
    setup() {

    }
}

ImageField.template = "sale_order.ImageField";
ImageField.components = {};

ImageField.props = {
    ...standardFieldProps,
};
ImageField.defaultProps = {};
ImageField.extractProps = ({ attrs }) => {
    return {

    };
};

ImageField.displayName = "";
ImageField.supportedTypes = [""];

registry.category("fields").add("image", ImageField);
```

## `创建Web服务`
点击菜单后，在弹出的输入框中输入服务名称，使用空格进行分割，例如`cos service`，完成后输入回车，会生成`static/src/services/cos_service.js`文件

生成的代码：
```js
/** @odoo-module */

import { registry } from "@web/core/registry";

export const cosService = {
    dependencies: ["rpc", "orm"],
    async start(env, { rpc, orm }) {

        return {  };
    },
};

registry.category("services").add("cosService", cosService);
```

## `获取导入路径`
选择`static/src/**/*`路径下的js文件，右键单击文件，点击`获取导入路径`菜单项，文件的导入路径会自动复制到剪切板。
* 注意：只有点击`static/src/**/*`路径下的`.js`类型的文件才会显示该菜单

比如点击`sale_order/static/src/services/cos_service.js`生成下列导入路径：
```js
import {  } from "@sale_order/services/cos_service";
```

## `打开导入文件`
- 配置：先配置Odoo模块的路径，打开vscode的设置，点击`工作区`，左侧菜单中找到扩展，然后在扩展列表中找到`Odoo Misc配置`，右侧配置编辑界面点击`Add Item`，将要搜索的Odoo模块路径添加进去。
- 使用：在JS文件中选中`"@web/views/fields/standard_field_props";`，然后点击鼠标右键，在菜单栏点击`Odoo Misc: 打开导入文件`即可打开导入文件
  - 注意：选中内容的开始与结尾要包含`"`