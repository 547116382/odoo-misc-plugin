# Change Log

## [1.0.3] - 2023-12-14

### Added

- 添加`打开导入文件`功能

## [1.0.1] - 2023-12-12

### Fixed

- 修复贪婪匹配导致的json解析问题
- 修复windows下文件路径分隔符的问题
- 修复MacOS下文件路径分隔符的问题

## [1.0.0] - 2023-12-12

- Initial release

### Added

- 创建Odoo模块
- 创建Model，根据模型名称创建Python代码以及视图代码
- 继承Model，根据模型名称创建Python代码
- 创建OWL组件，根据选择的类型生成不同的组件模板，支持的类型：action/field/common
- 创建Web服务，在`static/src/services`目录下生成服务代码模板
- 获取导入路径，选择Odoo模块`static/src/`目录下的js文件，可以自动生成导入路径
- 提供代码片段